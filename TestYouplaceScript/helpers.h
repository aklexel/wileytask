#ifndef _HELPERS_H 
#define _HELPERS_H

#include "globals.h"


// adds web_reg functions to extract params from test page
void add_test_web_reg_functions()
{
	web_reg_save_param_regexp (
		"ParamName=ShortAnswerQuestion",
		"RegExp=<p><input type=\"text\" name=\"([^\"]+)\"",
		"Notfound=warning",
		"Ordinal=All",
		SEARCH_FILTERS,
	    "Scope=Body",
		LAST );
	web_reg_save_param_regexp (
		"ParamName=MultipleChoiceQuestion",
		"RegExp=<p><(input type=\"radio\"|select)(.+)</p>",
		"Notfound=warning",
		"Ordinal=All",
		SEARCH_FILTERS,
	    "Scope=Body",
	    "Group=0",
		LAST );
	web_reg_find(
		"Text=Test successfully passed", 
		"SaveCount=TestPassed", 
		LAST );
}

// finds the longtest answer for a multiple-choice question
// and saves the value id of the answer to variable 'AnswerId'
//
// Parameters:
// question - a multiple-choice question string
// Examples of question strigns:
//<input type="radio" name="qvgV7R8C2a0mniB4" value="84j5DKt2">84j5DKt2 &nbsp;&nbsp;&nbsp;<input type="radio" name="qvgV7R8C2a0mniB4" value="evRW6GSR">evRW6GSR &nbsp;&nbsp;&nbsp;...
//<select name="TTg4nfme4mae78YN"><option value=""></option><option value="jd5h2j6GheHXaYIk">jd5h2j6GheHXaYIk</option><option value="4XnmTLi">4XnmTLi</option>...
void find_longest_answer(char *question)
{
	int i;
	char delimiters[] = "\"<", *id, *value;
	
	// extract answers (id and value) from question string
	lr_save_param_regexp (question,
		strlen(question),
		"RegExp=value=([^>]+>[^<]+)<",
		"Ordinal=All",
		"ResultParam=Answer",
		LAST );
	
	// find the longest answer
	lr_save_string("", "Answer");
	for (i = 1; i <= lr_paramarr_len("Answer"); i++)
	{
		// extract id and value from answer string (answer string example: "jd5h2j6GheHXaYIk">jd5h2j6GheHXaYIk)
		id = (char *)strtok(lr_paramarr_idx("Answer", i), delimiters);
		value = (char *)strtok(NULL, delimiters);
		
		if ( strlen(value) > strlen(lr_eval_string("{Answer}")) )
		{
			lr_save_string(value, "Answer");
			lr_save_string(id, "AnswerId");
		}
	}
}

// finds answers for multiple-choice questions (from parameter array 'MultipleChoiceQuestion')
// and saves the question id and the value id to parameter arrays 'QuestionId' and 'QuestionValueId'
void find_answers_for_multiple_choice_questions()
{
	int i;
	char name[30], *question;
	
	for (i = 1; i <= lr_paramarr_len("MultipleChoiceQuestion"); i++)
	{
		question = lr_paramarr_idx("MultipleChoiceQuestion", i);
		sprintf(name, "ResultParam=QuestionId_%d", i);
		lr_save_param_regexp (question,
			strlen(question),
			"RegExp=name=\"([^\"]+)\"",
			name,
			LAST );
		
		find_longest_answer(question);
		sprintf(name, "QuestionValueId_%d", i);
		lr_save_string(lr_eval_string("{AnswerId}"), name);
	}
	
	lr_save_int(i-1, "QuestionId_count");
	lr_save_int(i-1, "QuestionValueId_count");
}

// finds answers for short answer questions (from parameter array 'ShortAnswerQuestion')
// and saves the question id and the value id to parameter arrays 'QuestionId' and 'QuestionValueId'
void find_answers_for_short_answer_questions()
{
	int i, res_idx = lr_paramarr_len("QuestionId") + 1;
	char name[30];
	
	for (i = 1; i <= lr_paramarr_len("ShortAnswerQuestion"); i++,res_idx++)
	{
		sprintf(name, "QuestionId_%d", res_idx);
		lr_save_string( lr_paramarr_idx("ShortAnswerQuestion", i), name);
		
		sprintf(name, "QuestionValueId_%d", res_idx);
		lr_save_string( "test", name);
	}
	
	lr_save_int(res_idx-1, "QuestionId_count");
	lr_save_int(res_idx-1, "QuestionValueId_count");
}

// fill in form arguments for submitting answers for questions
// from parameter arrays 'QuestionId' and 'QuestionValueId'
//
// Parameters:
// p_form_args - a pointer to form arguments (t_lr_varargs)
void fill_submit_form_args(t_lr_varargs *p_form_args)
{
	int i, j;
	
	p_form_args->args[0] = ITEMDATA;
	for (i = 1, j = 1; i <= lr_paramarr_len("QuestionId"); i++)
	{
		lr_param_sprintf("tmp", "Name=%s", lr_paramarr_idx("QuestionId", i));
		p_form_args->args[j++] = lr_eval_string("{tmp}");
		
		lr_param_sprintf("tmp", "Value=%s", lr_paramarr_idx("QuestionValueId", i));
		p_form_args->args[j++] = lr_eval_string("{tmp}");
		p_form_args->args[j++] = ENDITEM;
	}
	p_form_args->args[j] = LAST;
}

// finds answers for questions (from parameter arrays 'ShortAnswerQuestion' and 'MultipleChoiceQuestion')
// and fill in form arguments for submitting answers for questions
//
// Parameters:
// p_form_args - a pointer to form arguments (t_lr_varargs)
void find_answers_for_questions(t_lr_varargs *p_form_args)
{
	find_answers_for_multiple_choice_questions();
	find_answers_for_short_answer_questions();
	fill_submit_form_args(p_form_args);
}

#endif // _HELPERS_H
