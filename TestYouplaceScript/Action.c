#include "globals.h"
#include "helpers.h"

Action()
{
	t_lr_varargs form_args;
	
	web_add_auto_header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	
	lr_start_transaction("S01_T01_PassTestSuccessfully");
	web_url("Open", "URL={TEST_URL}");
	
	add_test_web_reg_functions();
	web_link("Start test", "Text=Start test");
	
	while ( atoi(lr_eval_string("{TestPassed}")) == 0 )
	{
		find_answers_for_questions(&form_args);
		add_test_web_reg_functions();
		
		lr_think_time(5);
		
		lr_start_transaction("S01_T02_AnswerQuestion");
		web_submit_form("Submit", form_args);
		lr_end_transaction("S01_T02_AnswerQuestion", LR_AUTO);
	}
	
	lr_end_transaction("S01_T01_PassTestSuccessfully", LR_AUTO);
	return 0;
}
