#ifndef _GLOBALS_H
#define _GLOBALS_H

//--------------------------------------------------------------------
// Include Files
#include "lrun.h"
#include "web_api.h"
#include "lrw_custom_body.h"

//--------------------------------------------------------------------
// Global Types and Variables
typedef struct { char *args[50]; } t_lr_varargs;

#endif // _GLOBALS_H
